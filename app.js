/* Load Libraries */
const Discord = require('discord.js');
const winston = require('winston');

/* Load Events */
const events = {
	ready: require('./events/ready.js'),
	message: require('./events/message.js'),
};

/* Logger Init */
const logger = winston.createLogger({
	format: winston.format.printf(log => new Date().toISOString() + ` [${log.level.toUpperCase()}] - ${log.message}`),
	level: 'info',
	transports: [
		new winston.transports.Console(),
		new winston.transports.File({ filename: 'HiveBot.log' }),
	],
	// do not exit on handled exceptions
	exitOnError: false,
});


/* Bot Init */
const config = require('./config.json');
const bot = new Discord.Client();
bot.commands = new Discord.Collection();
bot.sounds = new Discord.Collection();

/* Event Init */
bot.on('message', message => { events.message(bot, message, logger); });
bot.on('ready', () => { events.ready(bot, logger); });
bot.on('debug', m => logger.log('debug', m));
bot.on('warn', m => logger.log('warn', m));
bot.on('error', m => logger.log('error', m));

process.on('uncaughtException', error => logger.log('error', error));


/* Bot Connect */
bot.login(config.apikeys.discord);