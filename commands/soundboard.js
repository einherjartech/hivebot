/*
 * Soundboard Command
 */


// Load required modules.
const fs = require('fs');
const Readable = require('stream').Readable
const config = require('../config.json');
const sounddir = config.sounddir;
let timer = null;

module.exports = {
	name: 'sb',
	description: 'Soundboard!',
	execute(message, args) {
		if (!args.length) {
			message.reply('You didnt provide any arguments.');
		}

		else if (args[0] == 'list') {
			message.channel.send(getSoundListFormatted(message.client.sounds));
			return;
		}
		else if (args[0] >= 0 && args[0] < message.client.sounds.length) {
			let voicechannel = message.member.voice.channel;
			if (!voicechannel) {
				message.reply('Please join a voice channel before trying again.');
			}
			else {
				let start = Date.now();
				voicechannel.join().then(connection => {
					var delta = Date.now() - start;
                    message.channel.send('Join time: ' + delta);
					let sound = message.client.sounds[args[0]];
					let volumePercent = config.volume;
					let maxVolume = 200;
					let minVolume = 5;
					if(args.length > 1) {
						volumePercent = Math.min(Math.max(minVolume, args[1]), maxVolume) / 100;
					}
					const dispatcher = connection.play(sounddir + sound.file);
					message.channel.send(`Playing from file ${sound.file} as requested by ${message.author.username}. Volume: ` + (volumePercent * 100) + "%");
					dispatcher.setVolume(volumePercent);
					dispatcher.on('finish', () => {
						if(timer) clearTimeout(timer);
						timer = setTimeout(
							() => {
								dispatcher.destroy();
								voicechannel.leave();
							}, config.soundboard_leave_timer);
					});
				});
			}
		}
		else if (args[0] > message.client.sounds.length) {
			message.reply('File not found, try a lower number.');
		}

		else if (!message.member.voice.channel) {
			message.reply('Please join a voice channel before using a sound command.');
		}

	},
};

function getSoundListFormatted(list) {
	let column1 = [];
	let column2 = [];
	let column3 = [];
	
	let currentColumn = 0;
	for (let i = 0; i < list.length; i++) {
		switch(currentColumn) {
			case 0:
				column1.push(list[i]);
				break;
			case 1:
				column2.push(list[i]);
				break;
			case 2:
				column3.push(list[i]);
				break;
		}

		currentColumn = ++currentColumn % 3;
	}

	let numRows = list.length / 3;

	let spaceBetweenColumns = 2;
	let column1Widest = getWidestFile(column1) + spaceBetweenColumns;
	let column2Widest = getWidestFile(column2) + spaceBetweenColumns;
	let column3Widest = getWidestFile(column3);

	let message = '';
	for(let row = 0; row < numRows; ++row) {
		if(row < column1.length) {
			message += padString(getSoundString(column1[row]), column1Widest);
		}
		if(row < column2.length) {
			message += padString(getSoundString(column2[row]), column2Widest);
		}
		if(row < column3.length) {
			message += padString(getSoundString(column3[row]), column3Widest) + '\n';
		}
	}
	return "`" + message.trim() + "`";
}

function getSoundString(sound) {
	return sound.index + '. ' + sound.file;
}

function getWidestFile(column) {
	let width = 0;

	for(let i = 0; i < column.length; ++i) {
		let length = getSoundString(column[i]).length;
		if(length > width) {
			width = length;
		}
	}
	return width;
}

function padString(string, width) {
	while(string.length < width) {
		string += ' ';
	}
	return string;
}