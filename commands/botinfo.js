/*
 * botinfo
 */

module.exports = {
	name: 'botinfo',
	description: 'Get information about the bot!',
	execute(message) {
		const data = [];
		data.push(message.client.user.username + ' is currently serving ' + message.client.guilds.size + ' communities and offering its services to ' + message.client.users.size + ' users.');
		message.channel.send(data, { split: true });
	},
};