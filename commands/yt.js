/*
 * Youtube Sound play
 */
// Load config for volume.
const config = require('../config.json');
// Load required libraries.
const ytdl = require('ytdl-core');

module.exports = {
	name: 'yt',
	description: 'Get information about the bot!',
	execute(message, args) {
		if (!message.member.voice.channel) {
			message.reply('Please join a voice channel before trying again.');
		}
		else {
			message.member.voice.channel.join().then(connection => {
				const stream = ytdl(args[0], { filter: 'audioonly', quality: 'highestaudio' });
				const dispatcher = connection.play(stream);
				dispatcher.setVolume(config.volume);
				dispatcher.on('finish', () => {
					dispatcher.destroy();
					message.member.voice.channel.leave();
				});
			});
		}
	},

};
