/*
 * sysinfo
 */

// Load dependencies.
const os = require('os');

// Set variables.
const cpus = os.cpus();
const cpuInfo = cpus[0];
let totalMem = os.totalmem() / 1048576;
totalMem = totalMem.toFixed(0);
let usedMem = totalMem - (os.freemem() / 1048576);
usedMem = usedMem.toFixed(0);

module.exports = {
	name: 'sysinfo',
	description: 'Get system info!',
	execute(message) {
		// Collect Data
		const data = [];
		data.push('**PLATFORM:** ' + os.platform);
		data.push('**ARCH:** ' + os.arch);
		data.push('**CPU MODEL:** ' + cpuInfo.model);
		data.push('**CPU SPEED:** ' + cpuInfo.speed);
		data.push('**MEMORY:** ' + usedMem + 'MB of ' + totalMem + 'MB');
		// Send Message.
		message.channel.send(data, { split: true });
	},
};