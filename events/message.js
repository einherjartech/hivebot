/**
 * Ready Event
 */
const config = require('../config.json');
const fs = require('fs');


module.exports = function(bot, message, logger) {
	if (!message.content.startsWith(config.prefix) || message.author.bot) return;
	const args = message.content.slice(config.prefix.length).split(/ +/);
	const command = args.shift().toLowerCase();
	if (!bot.commands.has(command)) return;

	try {
		bot.commands.get(command).execute(message, args);
		logger.info(`${command} command used by ${message.author.username}#${message.author.discriminator} in ${message.channel.guild.name}`);
	}
	catch (error) {
		console.error(error);
		message.reply('there was an error trying to execute that command!');
	}

};