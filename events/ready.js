﻿/**
 * Ready Event
 */
const config = require('../config.json');
const fs = require('fs');
const sounddir = config.sounddir;


module.exports = function(bot, logger) {
	/* Log start message */
	logger.info(`Bot is currently logged in as ${bot.user.username}#${bot.user.discriminator} and currently on ${bot.guilds.size} servers and serving ${bot.users.size} users`);

	/* Load commands */
	const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const command = require(`../commands/${file}`);

		// set a new item in the Collection
		// with the key as the command name and the value as the exported module
		bot.commands.set(command.name, command);
	}

	loadSounds(bot);

	/* Set game status */
	bot.user.setActivity(config.bot_status.activity, { type: config.bot_status.activity_type })
		.then(presence => logger.info(`Activity set ${presence.game ? presence.game.name : 'none'}`))
		.catch(console.error);

};

function loadSounds(bot) {
	bot.sounds = [];
	fs.readdirSync(sounddir).forEach((value, key) => {
		const sound = {};
		sound.index = key;
		sound.file = value;
		bot.sounds.push(sound);
	})
}